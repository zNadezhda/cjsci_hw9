import { EmailCheck } from './services/index';

const apiProvider = () => ({
  emailcheck: () => new EmailCheck(),
});

export { apiProvider };