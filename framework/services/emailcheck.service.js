import supertest from 'supertest';
import { urls } from '../config';

const EmailCheck = function EmailCheck() {
  this.get = async function check(accessKey, email) {
    const r = await supertest(urls.email).get(`/api/check?access_key=${accessKey}&email=${email}`);
    return r;
  };
};

export { EmailCheck };