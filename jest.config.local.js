module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default', 
    ["jest-html-reporters", {
      "publicPath": "./html-report",
      "filename": "report.html"
    }]
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};