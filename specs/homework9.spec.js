import { test } from '@jest/globals';
import { apiProvider } from '../framework';
import { context } from '../framework/config';

test('Метод возвращает корректные параметры отправленного email', async () => {
    const { body, status } = await apiProvider().emailcheck().get(context.accessKey, context.email);
    expect(status).toBe(200);
    expect(body.email).toBe(context.email.toLowerCase());
    expect(body.user).toBe(context.email.split('@')[0].toLowerCase());
    expect(body.domain).toBe(context.email.split('@')[1].toLowerCase());
});

describe('Параметр format_valid возвращается корректно', () => {
  test.each`
    email                   | valid
    ${'test'}               | ${false}
    ${'@yandex.ru'}         | ${false}
    ${'123@'}               | ${false}
    ${'test@ru'}            | ${false}
    ${'test@test@ya.ru'}    | ${false}
    ${'test@test.test'}     | ${true}
    ${'3@test.test'}        | ${true}
    ${'@@test.test'}        | ${false}
    ${'123@test.test.test'} | ${true}
    ${'test.test@1.test'}   | ${true}
  `('Значение email = $email валидно? - $valid', async ({ email, valid }) => {
    const { body, status } = await apiProvider().emailcheck().get(context.accessKey, email);
    expect(status).toBe(200);
    expect(body.email).toBe(email);
    expect(body.format_valid).toBe(valid);
  });
});

test('Возвращается ошибка с кодом = 101 при отправке запроса без accessKey', async () => {
  const { body, status } = await apiProvider().emailcheck().get('', context.email);
  expect(status).toBe(200);
  expect(body.success).toBe(false);
  expect(body.error.code).toBe(101);
  expect(body.error.type).toBe('missing_access_key');
});